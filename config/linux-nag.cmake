set(CMAKE_C_COMPILER gcc CACHE STRING "C compiler")
set(CMAKE_Fortran_COMPILER nagfor CACHE STRING "Fortran compiler")
set(CMAKE_Fortran_FLAGS_RELEASE "-O3 -DNDEBUG" CACHE STRING "Fortran flags for Release builds")
set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "-g -O3 -DNDEBUG" CACHE STRING "Fortran flags for RelWithDebInfo builds")
set(CMAKE_Fortran_FLAGS_DEBUG "-g -gline -C" CACHE STRING "Fortran flags for Debug builds")
