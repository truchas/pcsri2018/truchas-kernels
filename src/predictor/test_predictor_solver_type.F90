!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! Copyright (c) Los Alamos National Security, LLC.  This file is part of the
!! Pececillo code (C17093) and is subject to a modified BSD license; see the
!! file LICENSE located in the top-level directory of this distribution.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

program test_predictor_solver_type

  use,intrinsic :: iso_fortran_env, only: error_unit, output_unit
  use,intrinsic :: iso_c_binding, only: C_NEW_LINE
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use logging_services
  use timer_tree_type
  use unstr_mesh_type
  use mesh_geom_type
  implicit none

  integer :: status = 0, n, num_arg
  real(r8) :: lnorm_limit(2)
  character(255) :: arg
  character(:), allocatable :: prog, meshfile
  logical :: run_new = .true., run_old = .true.

  !! Get the program name from the command line.
  call get_command_argument(0, arg)
  n = scan(arg, '/', back=.true.)
  prog = trim(arg(n+1:))  ! remove the leading path component, if any

  !! Get the mesh file path from the command line.
  num_arg = command_argument_count()
  if (num_arg >=1 .and. num_arg <= 2) then
    call get_command_argument(1, arg)
    meshfile = trim(arg)
  else
    write(error_unit,'(3a)') 'usage: ', prog, ' MESHFILE [ old | new ]'
    stop 1
  end if

  !! Get the optional old/new argument
  if (num_arg == 2) then
    call get_command_argument(2, arg)
    if (arg == 'old') then ! only run the old kernel
      run_new = .false.
    else if (arg == 'new') then ! only run the new kernel
      run_old = .false.
    else
      write(error_unit,'(3a)') 'usage: ', prog, ' MESHFILE [ old | new ]'
      stop 1
    end if
  end if

  call LS_initialize([output_unit])

  lnorm_limit = 1e-10_r8
  call run_kernel

  !! Write some timing info.
  call LS_info (C_NEW_LINE//'Timing Summary:'//C_NEW_LINE)
  call write_timer_tree (output_unit, indent=3)

  print *, status
  if (status /= 0) stop 1

contains

  subroutine run_kernel

    use unstr_mesh_factory
    use predictor_solver_type
    use new_predictor_solver_type
    use parameter_list_type
    use parameter_list_json
    use matl_props_type

    type(unstr_mesh), pointer :: mesh
    type(mesh_geom) :: gmesh
    type(predictor_solver) :: predictor
    type(new_predictor_solver) :: new_predictor
    character(:), allocatable :: paramstr, errmsg
    type(parameter_list), pointer :: params => null()
    real(r8) :: err(2), dt
    real(r8), allocatable :: fluid_rho(:), fluid_rho_n(:), vof(:,:), fluid_vof(:), fluid_vof_n(:), &
        velocity_cc(:,:), velocity_cc_n(:,:), fluxing_velocity(:,:), volume_flux(:,:,:), &
        gradP_dynamic_over_rho_cc(:,:)
    logical, allocatable :: solid_face(:), is_pure_immobile(:)
    type(matl_props) :: mprop
    integer :: n, handle
    real :: time
    real, parameter :: time_max = 4.0
    integer, parameter :: iter_max = 1000
    integer, parameter :: mesh_size = 32

    ! initialize mesh
    call start_timer("initialization")
    call start_timer("mesh")
    ! mesh => new_unstr_mesh([-0.5_r8, -0.5_r8, -0.5_r8], [0.5_r8, 0.5_r8, 0.5_r8], &
    !     [mesh_size,mesh_size,mesh_size])
    mesh => new_unstr_mesh(meshfile)
    call gmesh%init(mesh)
    call stop_timer("mesh")

    ! initialize predictor
    call start_timer("predictor")
    call predictor%init(mesh, gmesh)
    call new_predictor%init(mesh, gmesh)
    call stop_timer("predictor")

    ! initialize fields & BCs
    call start_timer("fields")
    paramstr = '{"material-properties": {"densities": [1.0],"viscosities": [1.0]}}'
    call parameter_list_from_json_string(paramstr, params, errmsg)
    params => params%sublist('material-properties')
    call mprop%init(params)

    allocate(fluid_rho(mesh%ncell), fluid_vof(mesh%ncell), vof(1,mesh%ncell), &
        velocity_cc(NDIM,mesh%ncell), fluxing_velocity(NFC,mesh%ncell), &
        volume_flux(1,NFC,mesh%ncell), gradP_dynamic_over_rho_cc(NDIM,mesh%ncell), &
        solid_face(mesh%nface), is_pure_immobile(mesh%ncell))

    fluid_rho = mprop%density(1)
    call set_random_seed()
    call random_number(vof)
    call random_number(fluid_vof)
    call random_number(velocity_cc)
    call random_number(fluxing_velocity)
    call random_number(volume_flux)
    call random_number(gradP_dynamic_over_rho_cc)

    ! vof = 1
    ! fluid_vof = 1
    ! fluid_rho = mprop%density(1)

    ! velocity_cc = 0
    ! fluxing_velocity = 0
    ! volume_flux = 0
    ! gradP_dynamic_over_rho_cc = 0 / fluid_rho(1)

    ! ! x-dir stuff
    ! gradP_dynamic_over_rho_cc(1,:) = 1 / fluid_rho
    ! velocity_cc(1,:) = 1
    ! fluxing_velocity(4,:) = 1

    dt = 1 !0.5_r8 * (1/mesh_size) / maxval(abs(velocity_cc))
    volume_flux(1,:,:) = fluxing_velocity*dt * (1/mesh_size)**2

    solid_face = .false.
    is_pure_immobile = .false.
    fluid_rho_n = fluid_rho
    fluid_vof_n = fluid_vof
    velocity_cc_n = velocity_cc
    call stop_timer("fields")
    call stop_timer("initialization")

    ! solve predictor
    if (run_old) then
      call start_timer("old predictor kernel", handle)
      do n = 1, iter_max
        call predictor%compute_system(velocity_cc, gradP_dynamic_over_rho_cc, dt, volume_flux, &
            fluid_rho, fluid_rho_n, vof, fluid_vof, fluid_vof_n, velocity_cc_n, fluxing_velocity, &
            0.5_r8, solid_face, is_pure_immobile, mprop)
        call read_timer(handle, time)
        if (time > time_max) exit
      end do
      write(output_unit,'(i0,a)') n, ' iterations of the old kernel'
      call stop_timer("old predictor kernel")
    end if

    if (run_new) then
      call start_timer("new predictor kernel", handle)
      do n = 1, iter_max
        call new_predictor%compute_system(gradP_dynamic_over_rho_cc, dt, volume_flux, fluid_rho, &
            fluid_rho_n, vof, fluid_vof, fluid_vof_n, velocity_cc_n, solid_face, is_pure_immobile, &
            mprop)
        call read_timer(handle, time)
        if (time > time_max) exit
      end do
      write(output_unit,'(i0,a)') n, ' iterations of the new kernel'
      call stop_timer("new predictor kernel")
    end if

    ! calculate error norms
    if (run_old .and. run_new) then
      err = system_error(predictor, new_predictor)
      print '(a,2es13.3)', 'lhs error, rhs error = ', err

      if (any(err > lnorm_limit)) status = 1
    end if

  end subroutine run_kernel

  ! returns the maximum error in the lhs (component 1) and the rhs (component 2)
  function system_error(predictor, new_predictor)

    use predictor_solver_type
    use new_predictor_solver_type

    type(predictor_solver), intent(in) :: predictor
    type(new_predictor_solver), intent(in) :: new_predictor
    real(r8) :: system_error(2)

    INSIST(size(predictor%lhs%values) == size(new_predictor%lhs%values))
    INSIST(size(predictor%rhs) == size(new_predictor%rhs))

    ! lhs & rhs error
    system_error(1) = maxval(abs(predictor%lhs%values - new_predictor%lhs%values))
    system_error(2) = maxval(abs(predictor%rhs - new_predictor%rhs))

  end function system_error

  ! the nag compiler does not seem to have a deteriministic seed by default, so use this.
  subroutine set_random_seed()

    integer :: n
    integer, allocatable :: seed(:)

    call random_seed(size=n)

    allocate(seed(n))
    seed = 0

    call random_seed(put=seed)

  end subroutine set_random_seed

end program test_predictor_solver_type
