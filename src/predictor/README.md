Predictor Kernel
----------------
This computational kernel calculates the linear system for the predictor step in
a typical incompressible flow scheme, returning the left hand side matrix and
right hand side vector.

The driver program takes no arguments, and will automatically generate a
Cartesian mesh using the unstructured mesh data type. It generates random input
data in the relevant fields, then calls the original and new predictor
procedures (`predictor_solver_type.F90`, `new_predictor_solver_type.F90`) on
that data. Finally it compares the two outputs.

To start, `new_predictor_solver_type.F90` is an exact copy of the original
code, with a few name changes to avoid name clashes. The intent is that you will
modify this copy.
