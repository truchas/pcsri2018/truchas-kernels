Mimetic Finite Difference Diffusion Kernel
------------------------------------------
This computational kernel forms the diffusion operator for the mimetic finite
difference (MFD) discretization on an unstructured mesh.

The driver program reads a mesh (mesh file specified on the command line) and
instantiates the unstructured mesh object. It first calls the original diffusion
operator procedure with random input data (`mfd_disc_type.F90`) to get the
'correct' reference output. It then calls the modified diffusion operator
procedure with the same random input data (`new_mfd_disc_type.F90`) and
compares its output with the reference output.

To start, `new_mfd_disc_type.F90`
is an exact copy of the original code, with a few name changes to avoid name
clashes. The intent is that you will modify this copy.
