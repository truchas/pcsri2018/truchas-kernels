set(SRC
    main.F90
    mfd_disc_type.F90
    upper_packed_matrix.F90
    new_mfd_disc_type.F90
    new_upper_packed_matrix.F90
)

add_executable(mfd-diff-test ${SRC})
target_link_libraries(mfd-diff-test common)

add_test(mfd-diff-test1 mfd-diff-test ${CMAKE_SOURCE_DIR}/meshes/mesh1-fine.exo)
