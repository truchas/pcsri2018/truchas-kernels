program mfd_diff_test

  use,intrinsic :: iso_fortran_env, only: error_unit, output_unit
  implicit none

  character(255) :: arg
  character(:), allocatable :: prog, meshfile
  integer :: n, num_arg
  logical :: pass = .true., run_new = .true., run_old = .true.

  !! Get the program name from the command line.
  call get_command_argument(0, arg)
  n = scan(arg, '/', back=.true.)
  prog = trim(arg(n+1:))  ! remove the leading path component, if any

  !! Get the mesh file path from the command line.
  num_arg = command_argument_count()
  if (num_arg >=1 .and. num_arg <= 2) then
    call get_command_argument(1, arg)
    meshfile = trim(arg)
  else
    write(error_unit,'(3a)') 'usage: ', prog, ' MESHFILE'
    stop 1
  end if

  !! Get the optional old/new argument
  if (num_arg == 2) then
    call get_command_argument(2, arg)
    if (arg == 'old') then ! only run the old kernel
      run_new = .false.
    else if (arg == 'new') then ! only run the new kernel
      run_old = .false.
    else
      write(error_unit,'(3a)') 'usage: ', prog, ' MESHFILE [ old | new ]'
      stop 1
    end if
  end if

  call run_kernel

  if (.not.pass) stop 1 ! return error exit status

contains

  subroutine run_kernel

    use,intrinsic :: iso_fortran_env, only: r8 => real64
    use unstr_mesh_type
    use unstr_mesh_factory
    use mfd_disc_type
    use new_mfd_disc_type
    use timer_tree_type

    integer :: n, handle
    real :: time, mintime = 2.0
    type(unstr_mesh), pointer :: mesh
    type(mfd_disc) :: old_mfd
    type(new_mfd_disc) :: new_mfd
    real(r8), allocatable :: coef(:), ucell(:), old_rcell(:), new_rcell(:)
    real(r8), allocatable :: uface(:), old_rface(:), new_rface(:)
    real(r8) :: err_cell, err_face, err, errtol = 1.0d-12
    type(timer_tree) :: timer

    !! Instantiate the unstructured mesh object.
    call timer%start('mesh initialization')
    mesh => new_unstr_mesh(meshfile)
    write(output_unit,'(3(i0,a))') mesh%ncell, ' cells, ', mesh%nnode, ' nodes, ', mesh%nface, ' faces'
    call timer%stop('mesh initialization')

    !! Initialize the input arrays with random data.
    allocate(coef(mesh%ncell), ucell(mesh%ncell), old_rcell(mesh%ncell), new_rcell(mesh%ncell))
    allocate(uface(mesh%nface), old_rface(mesh%nface), new_rface(mesh%nface))
    !call timer%start('array initialization')
    call random_number(coef)
    call random_number(ucell)
    call random_number(uface)
    !call timer%stop('array initialization')

    if (run_old) then
      !! Initialize the original MFD discretization object.
      call timer%start('old mfd_disc%init', handle)
      do n = 1, 5000
        call old_mfd%init(mesh)
        call timer%read(handle, time)
        if (time > mintime) exit
      end do
      call timer%stop('old mfd_disc%init')
      write(output_unit,'(i0," iterations of old mfd_disc%init")') n

      !! Apply the original MFD diffusion operator.
      call timer%start('old mfd_disc%apply_diff', handle)
      do n = 1, 1000
        call old_mfd%apply_diff(coef, ucell, uface, old_rcell, old_rface)
        call timer%read(handle, time)
        if (time > mintime) exit
      end do
      call timer%stop('old mfd_disc%apply_diff')
      write(output_unit,'(i0," iterations of old mfd_disc%apply_diff")') n
    end if

    if (run_new) then
      !! Initialize the new MFD discretization object.
      call timer%start('new mfd_disc%init', handle)
      do n = 1, 5000
        call new_mfd%init(mesh)
        call timer%read(handle, time)
        if (time > mintime) exit
      end do
      call timer%stop('new mfd_disc%init')
      write(output_unit,'(i0," iterations of new mfd_disc%init")') n

      !! Apply the new MFD diffusion operator.
      call timer%start('new mfd_disc%apply_diff', handle)
      do n = 1, 5000
        call new_mfd%apply_diff(coef, ucell, uface, new_rcell, new_rface)
        call timer%read(handle, time)
        if (time > mintime) exit
      end do
      call timer%stop('new mfd_disc%apply_diff')
      write(output_unit,'(i0," iterations of new mfd_disc%apply_diff")') n
    end if

    if (run_old .and. run_new) then ! compare results
      n = maxloc(abs(old_rcell-new_rcell),dim=1)
      err_cell = abs(old_rcell(n)-new_rcell(n))
      write(output_unit,'(/,a,es13.5,a,i0)') 'max cell error =', err_cell, ' at cell ', n
      n = maxloc(abs(old_rface-new_rface),dim=1)
      err_face = abs(old_rface(n)-new_rface(n))
      write(output_unit,'(a,es13.5,a,i0)') 'max face error =', err_face, ' at face ', n
      err = max(err_cell, err_face)
      pass = (err <= errtol)
      write(output_unit,'(a,es13.5,2x,a)') 'max error =', err, merge('PASS','FAIL',pass)
    end if

    write(output_unit,*)
    call timer%write(output_unit, indent=2)

    deallocate(mesh)

  end subroutine run_kernel

end program mfd_diff_test
