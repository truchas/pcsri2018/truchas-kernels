set(SRC
  interface_normal_function.F90
  new_interface_normal_function.F90
  f08_intrinsics.F90
)

add_executable(int-norm-test ${SRC} test_interface_normal_function.F90)
target_link_libraries(int-norm-test common)

add_executable(int-norm-driver ${SRC} int_norm_driver.F90)
target_link_libraries(int-norm-driver common)

add_test(int-norm-test1 int-norm-test ${CMAKE_SOURCE_DIR}/meshes/mesh1-fine.exo)
