Interface Normal Kernel
-----------------------
This computational kernel calculates the normalized gradient of a scalar field
on an unstructured mesh.

The driver program takes no arguments, and will automatically generate a series
of Cartesian meshes using the unstructured mesh data type. It then calls the new
interface normal function (`new_interface_normal_function.F90`) on linear and
Gaussian fields for which the exact solution is known, and prints to the
terminal error norms, ignoring cells touching the boundary. Finally, it runs the
new interface normal function on random input data and compares the output to
the same calculation performed by the original interface normal function
(`interface_normal_function.F90`).

To start, `new_interface_normal_function.F90` is an exact copy of the original
code, with a few name changes to avoid name clashes. The intent is that you will
modify this copy.
