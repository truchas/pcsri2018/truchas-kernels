module new_interface_normal_function

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use unstr_mesh_type
  use mesh_geom_type
  use timer_tree_type
  implicit none
  private

  public :: compute_interface_normal  ! better subroutine interface
  public :: new_interface_normal  ! function interface like original code

contains

  !! Function flavor, like original, called by test_interface_normal_function program
  function new_interface_normal (vof, mesh, gmesh) result(interface_normal)

    real(r8),intent(in) :: vof(:)
    type(unstr_mesh), intent(in) :: mesh
    type(mesh_geom),  intent(in) :: gmesh
    real(r8):: interface_normal(3,mesh%ncell)

    call start_timer("normal calculation")
    call compute_interface_normal(mesh, gmesh, vof, interface_normal)
    call stop_timer("normal calculation")

  end function new_interface_normal


  !! Better subroutine flavor called by int_norm_driver program.
  subroutine compute_interface_normal(mesh, gmesh, u_cell, normal)

    type(unstr_mesh), intent(in) :: mesh
    type(mesh_geom),  intent(in) :: gmesh
    real(r8), intent(in) :: u_cell(:)
    real(r8), intent(out) :: normal(:,:)

    integer :: i, j, k, n
    real(r8) :: sum1, sum2, tmp, gradient(3)
    real(r8) :: u_node(mesh%nnode), u_face(mesh%nface), w(mesh%nnode)

!    !! Reciprocal volume weighted averaging to nodes (node loop method)
!    call start_timer('average to nodes')
!    do j = 1, mesh%nnode
!      sum1 = 0.0_r8; sum2 = 0.0_r8
!      do i = 1, size(gmesh%vcell,dim=1)
!        n = gmesh%vcell(i,j)
!        if (n <= 0) exit
!        sum1 = sum1 + u_cell(n)*(1.0_r8/mesh%volume(n))
!        sum2 = sum2 + (1.0_r8/mesh%volume(n))
!      end do
!      u_node(j) = sum1/sum2
!    end do
!    call stop_timer('average to nodes')

    !! Reciprocal volume weighted averaging to nodes (cell loop method)
    call start_timer('average to nodes')
    u_node = 0.0_r8; w = 0.0_r8
    do j = 1, mesh%ncell
      do k = 1, size(mesh%cnode,dim=1)
        n = mesh%cnode(k,j)
        u_node(n) = u_node(n) + u_cell(j)*(1.0_r8/mesh%volume(j))
        w(n) = w(n) + (1.0_r8/mesh%volume(j))
      end do
!      !! Alternative for the do k loop that uses compact array syntax
!      associate (nodes => mesh%cnode(:,j))
!        u_node(nodes) = u_node(nodes) + u_cell(j)*(1.0_r8/mesh%volume(j))
!        w(nodes) = w(nodes) + (1.0_r8/mesh%volume(j))
!      end associate
    end do
    u_node = u_node/w
    call stop_timer('average to nodes')

    !! Averaging to faces (skip for Alternative 1 and 3 below)
    call start_timer('average to faces')
    do j = 1, mesh%nface
      u_face(j) = sum(u_node(mesh%fnode(:,j)))/size(mesh%fnode,dim=1)
    end do
    call stop_timer('average to faces')

    !! Scale average gradient from Gauss-Green theorem to get interface normal.
    call start_timer('gauss-green')
    do j = 1, mesh%ncell
      gradient = 0.0_r8
      do k = 1, size(mesh%cface,dim=1)
        n = mesh%cface(k,j)
        gradient = gradient + (u_face(n)*mesh%area(n))*gmesh%outnorm(:,k,j)
!        !! Alternative 1: compute face avg on the fly instead of precomputing (u_face loop above)
!        tmp = sum(u_node(mesh%fnode(:,n)))/size(mesh%fnode,dim=1)
!        gradient = gradient + (tmp*mesh%area(n))*gmesh%outnorm(:,k,j)
!        !! Alternative 2: get the normal from the face-centered mesh%normal array
!        tmp = 1.0_r8
!        if (btest(mesh%cfpar(j),pos=k)) tmp = -1.0_r8
!        gradient = gradient + (tmp*u_face(n))*mesh%normal(:,n)
!        !! Alternative 3: combine Alternatives 1 and 2
!        tmp = sum(u_node(mesh%fnode(:,n)))/size(mesh%fnode,dim=1)
!        if (btest(mesh%cfpar(j),pos=k)) tmp = -tmp
!        gradient = gradient + tmp*mesh%normal(:,n)
      end do
      !gradient = gradient/mesh%volume(j) ! pointless due to following normalization
      tmp = norm2(gradient)
      if (tmp > epsilon(1.0_r8)*mesh%volume(j)) then  ! to match original
        normal(:,j) = -gradient/tmp
      else
        normal(:,j) = 1.0_r8
      end if
    end do
    call stop_timer('gauss-green')

  end subroutine compute_interface_normal

end module new_interface_normal_function
