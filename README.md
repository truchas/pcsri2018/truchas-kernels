Truchas computational kernels for the 2018 PCSRI project
========================================================
This repository contains several key computational kernels extracted from the
Truchas and Pececillo codes. (Pececillo is a mini-app for Truchas.) The goal
of the summer project is to investigate, assess, and compare different
approaches for parallel optimization of the kernels.

Manifest
--------
Here's a brief outline of how the repository is organized.

* `src/int-normal`. Computation of the approximate normal to the level sets
  of a cell-centered volume fraction field extracted from the fluid flow interface reconstruction solver. Includes a driver program for the kernel.

* `src/predictor`. Computation of the matrix and rhs of the velocity predictor
  system from the fluid flow solver, with a driver program for the kernel.

* `src/mfd-diff`. Computation of the mimetic finite difference diffusion
  operator from the heat transfer solver, with a driver program for the kernel.

* `src/common`. Common code used primarily by the driver programs, and to a
  lesser extent by the computational kernels.

* `meshes`. Test meshes for use with the kernel driver programs.

* `cmake`. Contains some CMake modules used by the CMake build system (all the
  `CMakeLists.txt` files.)

* `config`. CMake configuration files for use when running `cmake`.

Getting Started
---------------
Some *very* brief instructions on how to checkout a copy of the repository to
your local workspace, compile it, and run some tests. This uses `git`, `cmake`,
and `make`

### Prequisites
You first need to compile and install the third party libraries (TPL) that
the code in this repository relies upon. See the `README.md` file at the
separate project https://gitlab.com/truchas/pcsri2018/truchas-kernels-tpl
for instructions.

### Cloning

    git clone git@gitlab.com:truchas/pcsri2018/truchas-kernels.git

This creates a `truchas-kernels` subdirectory that contains a clone of the
git repository (hidden in the `.git` subdirectory) and a checkout of its master
branch.

### Compiling

If you are configured to use the Intel compilers (i.e., you've loaded the Intel compiler module), then you'd compile like this

    cd truchas-kernels
    mkdir build
    cmake -C ../config/intel-opt.cmake -DCMAKE_PREFIX_PATH=/tpl/install/dir ..
    make

Here `/tpl/install/dir` is the directory where your TPLs where installed.
Don't overlook the `..` at the end of the command.

The executables are located down in `/build/src/mfd-diff`, and so on.

Note: The current version of `mfd_diff_type.F90` triggers an internal compiler
error with Intel 17.0.6. It is most likely due to addition of some contiguous
attributes. Let's just stick with version 18.0.2 or later which works fine.

### Testing

From the `build` directory

    ctest

will run several tests, which were defined in the the `CMakeFiles.txt` files.
We'll want to add more.
